if (typeof ORG == undefined || !ORG) {

    var ORG = {};
}
if (!ORG.components) {
    ORG.components = {};
}

(function() {
    var $siteURL = Alfresco.util.siteURL;
    ORG.components.DocumentList = function CustomDocumentList_constructor(
        htmlId) {
        ORG.components.DocumentList.superclass.constructor.call(this, htmlId);
        return this;
    };

    YAHOO
        .extend(
            ORG.components.DocumentList, Alfresco.DocumentList, {
                onReady: function CustomDL_onReady() {
                    ORG.components.DocumentList.superclass.onReady.call(this);
                    var elms = Dom.getElementsByClassName("left");
                    var spn = document.createElement('span');
                    var btn = document.createElement('button');
                    var t = document.createTextNode("Custom Content");
                    btn.appendChild(t);
                    btn.onclick = this.onButtonClick;
                    spn.setAttribute('class','yui-button yui-push-button');
                    spn.appendChild(btn);
                    elms[0].appendChild(spn);
                },
                onButtonClick: function CustomDL_onButtonClick(e,p_obj) {
                	var toolbar = Alfresco.util.ComponentManager.findFirst("Alfresco.DocListToolbar");
                	var destination = toolbar.doclistMetadata.parent.nodeRef;
                	 Alfresco.util.navigateTo($siteURL("create-content?itemId=myc:marketingDoc&isContainer=true&destination="+destination), "GET",
                	         {
                	            itemId: "myc:marketingDoc",
                	            destination: destination
                	         });
                	
                	/*
                    var toolbar = Alfresco.util.ComponentManager.findFirst("Alfresco.DocListToolbar");
                    var actions = toolbar.modules.docList;
                    var destination = toolbar.doclistMetadata.parent.nodeRef;
                    var doBeforeDialogShow = function DLTB_onNewFolder_doBeforeDialogShow(p_form, p_dialog) {
                        Dom.get(p_dialog.id + "-dialogTitle").innerHTML = "Header";
                        Dom.get(p_dialog.id + "-dialogHeader").innerHTML = "Title";
                    };
                    var templateUrl = YAHOO.lang
                        .substitute(Alfresco.constants.URL_SERVICECONTEXT +"components/form?itemKind={itemKind}&itemId={itemId}&destination={destination}&mode={mode}&submitType={submitType}&formId={formId}&showCancelButton=true", {
                                itemKind: "type",
                                itemId: "myc:marketingDoc",
                                destination: destination,
                                mode: "create",
                                submitType: "json",
                                formId: "doclib-common"
                            });
                    var createFolder = new Alfresco.module.SimpleDialog(toolbar.id + "-createFolder");
                    createFolder.setOptions({
                        width: "58em",
                        templateUrl: templateUrl,
                        actionUrl: null,
                        destroyOnHide: true,
                        doBeforeDialogShow: {
                            fn: doBeforeDialogShow,
                            scope: this
                        },
                        onSuccess: {
                            fn: function DLTB_onNewFolder_success(
                                response) {
                                var activityData;
                                var folderName = response.config.dataObj["prop_name"];
                                var folderNodeRef = response.json.persistedObject;
                                activityData = {
                                    fileName: folderName,
                                    nodeRef: folderNodeRef,
                                    path: toolbar.currentPath + (toolbar.currentPath !== "/" ? "/" :"") +folderName
                                };
                                toolbar.modules.actions
                                    .postActivity(
                                        toolbar.options.siteId,
                                        "folder-added",
                                        "documentlibrary",
                                        activityData);

                                YAHOO.Bubbling
                                    .fire("folderCreated", {
                                        name: folderName,
                                        parentNodeRef: destination
                                    });
                                Alfresco.util.PopupManager
                                    .displayMessage({
                                        text: toolbar
                                            .msg(
                                                "message.new-folder.success",
                                                folderName)
                                    });
                            },
                            scope: this
                        },
                        onFailure: {
                            fn: function DLTB_onNewFolder_failure(
                                response) {
                                if (response) {
                                    var folderName = response.config.dataObj["prop_name"];
                                    Alfresco.util.PopupManager
                                        .displayMessage({
                                            text: toolbar
                                                .msg(
                                                    "message.new-folder.failure",
                                                    folderName)
                                        });
                                } else {
                                    Alfresco.util.PopupManager
                                        .displayMessage({
                                            text: toolbar
                                                .msg("message.failure")
                                        });
                                }
                                createFolder.widgets.cancelButton
                                    .set("disabled", false);
                            },
                            scope: this
                        }
                    });
                    createFolder.show();
                    return createFolder;
                */}
            });
})();